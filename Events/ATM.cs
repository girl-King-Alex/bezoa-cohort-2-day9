﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Day_7.Model;

namespace Day_7.Events
{
   public class ATM
   {

        public event EventHandler<WithdrawalViewModel> Withdrawal;
        public event EventHandler<BalanceCheckViewModel> BalanceCheck;
        public event EventHandler<TransferViewModel> Send;

        public void Withdraw(string account, int amount)
        {
            Console.WriteLine("Dispensing Cash....");
            Console.WriteLine($"Account: {account}");
            Console.WriteLine($"Amount: {amount}");

            OnWithdrawal(new WithdrawalViewModel {Account = account, Amount = amount});
        }

        public void CheckBalance(string account)
        {
            Console.WriteLine($"Account:{account}\nYour Current Balance as of {DateTime.Now} is {1200:C}");
            OnBalanceCheck(new BalanceCheckViewModel{Account = account });
        }


        public void Transfer(int amount, string senderAccount,  string destAccount)
        {
            Console.WriteLine($"Transfer was Successful....");
            Console.WriteLine($"Details:\nAmount:{amount}\nType of Transaction: {nameof(Transfer)}\nFrom:{senderAccount}\nTo:{destAccount}");
            OnSend(new TransferViewModel{Amount = amount, DestAccount = destAccount, SenderAccount = senderAccount });
        }

        protected virtual void OnWithdrawal(WithdrawalViewModel model)
        {
            Withdrawal?.Invoke(this, model);
        }

        protected virtual void OnBalanceCheck(BalanceCheckViewModel model)
        {
            BalanceCheck?.Invoke(this,  model);
        }

        protected virtual void OnSend(TransferViewModel model)
        {
            Send?.Invoke(this, model);
        }
    }
}
