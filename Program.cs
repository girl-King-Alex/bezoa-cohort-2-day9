﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Day_7.Events;

namespace Day_7
{
    class Program
    {
        static void Main(string[] args)
        {
            // var items = new String[]{"pen", "book", "mac book"};
            //
            //
            // var containsUpperCaseItems = items.Any(item => item.Equals(item.ToUpper()));
            //
            // Console.WriteLine(containsUpperCaseItems);

            
            var atm = new ATM();
            var accountService = new AccountService();

            //Subscription
            atm.Withdrawal += accountService.RecordWithdrawal;
            atm.BalanceCheck += accountService.RecordBalance;
            atm.Send += accountService.RecordTransfer;

            
            atm.Withdraw("0086535271", 15_000_000);
            atm.Transfer(15_000_000, "0086535271", "0081435271");
            atm.CheckBalance("0086535271");
        }
    }
}
